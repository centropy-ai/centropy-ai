package database

import (
	"fmt"

	"gitlab.com/centropy-ai/centropy-ai/database/gorm"
)

type Config struct {
	Host     string
	Port     int
	Name     string
	User     string
	Password string
	Migrate  string
	Debug    bool
	Dialect  string
}

const (
	postgres = "postgres"
	mysql    = "mysql"
)

func (c Config) DSN() (string, gorm.DSN) {
	if c.Dialect == postgres {
		dsn := fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=disable",
			c.Host,
			c.Port,
			c.User,
			c.Name,
			c.Password,
		)
		return c.Dialect, gorm.DSN(dsn)
	}

	if c.Dialect == mysql {
		//mysql
		dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True",
			c.User,
			c.Password,
			c.Host,
			c.Port,
			c.Name,
		)
		return c.Dialect, gorm.DSN(dsn)
	}
	return "", gorm.DSN("")
}
