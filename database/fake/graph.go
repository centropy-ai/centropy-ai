package fake

import (
	"context"
	"testing"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/centropy-ai/centropy-ai/ent"
)

// PrepareGraphForTesting prepares database for testing
// includes creating database engine and database schemas of this project.
func PrepareGraphForTesting(t *testing.T) *ent.Client {
	mdb, err := ent.Open("sqlite3", "file::memory:?cache=private&_fk=1")
	if err != nil {
		t.Fatal(err)
	}
	if err := mdb.Schema.Create(context.Background()); err != nil {
		t.Fatal(err)
	}
	return mdb
}
