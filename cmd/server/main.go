package main

import (
	"gitlab.com/centropy-ai/centropy-ai/cmd"
)

func main() {
	cmd.Execute()
}
