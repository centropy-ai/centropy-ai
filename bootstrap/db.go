package bootstrap

import (
	"context"

	"gitlab.com/centropy-ai/centropy-ai/config"
	"gitlab.com/centropy-ai/centropy-ai/database"
	"gitlab.com/centropy-ai/centropy-ai/database/gorm"
	"gitlab.com/centropy-ai/centropy-ai/ent"
	"gitlab.com/centropy-ai/centropy-ai/migration"
)

var (
	dbEngine  database.DBEngine
	entClient *ent.Client
)

func LoadGraphDB(cfg *config.Database) {
	if cfg == nil {
		panic("config: nil")
	}
	dbcfg := database.Config(*cfg)
	dialect, dsn := dbcfg.DSN()
	client, err := ent.Open(dialect, dsn.String())
	if err != nil {
		panic(err)
	}
	err = client.Schema.Create(context.Background())
	if err != nil {
		panic(err)
	}
	entClient = client
}
func LoadDB(cfg *config.Database) {
	if cfg == nil {
		panic("config: nil")
	}
	dbcfg := database.Config(*cfg)
	err := runMigration(dbcfg)
	if err != nil {
		panic(err)
	}

	dbEngine, err = database.Open(dbcfg)
	if dbcfg.Debug {
		dbEngine.Debug()
		dbEngine.LogMode(true)
	}
	if err != nil {
		panic(err)
	}
}

func runMigration(cfg database.Config) error {
	db, err := gorm.OpenDialects(cfg.DSN())
	db.LogMode(true)
	if err != nil {
		return nil
	}
	defer db.Close()
	m := migration.New(migration.WithGormigrate(db, cfg.Migrate))
	return m.Migrate()
}
