package bootstrap

import (
	"os"
	"sync"

	"gitlab.com/centropy-ai/centropy-ai/config/env"
	"gitlab.com/centropy-ai/centropy-ai/config/loader"
	"gitlab.com/centropy-ai/centropy-ai/config/storage"
)

var (
	once      sync.Once
	cfgLoader loader.Loader
)

func initialize() {
	once.Do(func() {
		s := storage.FromString(os.Getenv(env.KeyConfigStorage))

		cfgLoader = loader.New(s)
	})
}

func init() {
	initialize()
}
