package util

import (
	"fmt"

	structpb "github.com/golang/protobuf/ptypes/struct"
)

type ElementBuilder func(string, interface{})

func FlatMap(prefix string, data map[string]interface{}, b ElementBuilder) {
	protoStruct := StructProto(data)
	for k, v := range protoStruct.Fields {
		decodeValueWithPrefix(fmt.Sprintf("%s%s", prefix, k), v, b)
	}
}

func decodeValueWithPrefix(key string, v *structpb.Value, b ElementBuilder) {
	switch k := v.Kind.(type) {
	case *structpb.Value_NullValue:
		break
	case *structpb.Value_NumberValue:
		b(key, k.NumberValue)
	case *structpb.Value_StringValue:
		b(key, k.StringValue)
	case *structpb.Value_BoolValue:
		var final float64
		if k.BoolValue {
			final = 1
		}
		b(key, final)
	case *structpb.Value_StructValue:
		for inKey, v := range k.StructValue.Fields {
			if v != nil {
				decodeValueWithPrefix(fmt.Sprintf("%s.%s", key, inKey), v, b)
			}
		}
	case *structpb.Value_ListValue:
		for i, e := range k.ListValue.Values {
			decodeValueWithPrefix(fmt.Sprintf("%s.%d", key, i), e, b)
		}
	}
}
