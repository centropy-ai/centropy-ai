package util

import (
	"encoding/json"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFlatMap(t *testing.T) {
	type args struct {
		data map[string]interface{}
	}
	tests := []struct {
		name string
		args args
		want map[string]interface{}
	}{
		{
			name: "#1: Simple Data",
			args: args{
				data: func() map[string]interface{} {
					raw := `{"id":1,"name":"Paul"}`
					arg := make(map[string]interface{})
					_ = json.Unmarshal([]byte(raw), &arg)
					return arg
				}(),
			},
			want: map[string]interface{}{"id": 1.0, "name": "Paul"},
		},
		{
			name: "#2: Nested Data",
			args: args{
				data: func() map[string]interface{} {
					raw := `{"id":1,"name":"Paul","family":{"father":"Peter"}}`
					arg := make(map[string]interface{})
					_ = json.Unmarshal([]byte(raw), &arg)
					return arg
				}(),
			},
			want: map[string]interface{}{"id": 1.0, "name": "Paul", "family.father": "Peter"},
		},
		{
			name: "#3: Nested Data with array.string",
			args: args{
				data: func() map[string]interface{} {
					raw := `{"id":1,"name":"Paul","family":{"brother":["Mathew", "John"]}}`
					arg := make(map[string]interface{})
					_ = json.Unmarshal([]byte(raw), &arg)
					return arg
				}(),
			},
			want: map[string]interface{}{"id": 1.0, "name": "Paul", "family.brother.0": "Mathew", "family.brother.1": "John"},
		},
		{
			name: "#4: Nested Data with array.int",
			args: args{
				data: func() map[string]interface{} {
					raw := `{"id":1,"name":"Paul","family":{"brother":[12,15]}}`
					arg := make(map[string]interface{})
					_ = json.Unmarshal([]byte(raw), &arg)
					return arg
				}(),
			},
			want: map[string]interface{}{"id": 1.0, "name": "Paul", "family.brother.0": 12.0, "family.brother.1": 15.0},
		},
		{
			name: "#5: Nested Data with bool and null",
			args: args{
				data: func() map[string]interface{} {
					raw := `{"id":1,"name":"Paul","family":{"brother":[{"name":"Peter"}],"best": true}}`
					arg := make(map[string]interface{})
					_ = json.Unmarshal([]byte(raw), &arg)
					return arg
				}(),
			},
			want: map[string]interface{}{"id": 1.0, "name": "Paul", "family.brother.0.name": "Peter", "family.best": 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := make(map[string]interface{})
			builder := func(key string, val interface{}) {
				got[key] = val
			}
			if FlatMap("", tt.args.data, builder); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FlatMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

var (
	rawJSONTest = `{
  "swagger": "2.0",
  "info": {
    "title": "backend.proto",
    "version": 1.2
  },
  "schemes": [
    "http",
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/v1/version": {
      "get": {
        "summary": "Version",
        "operationId": "Version",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/protoVersionResponse"
            }
          }
        },
        "tags": [
          "Backend"
        ]
      }
    }
  },
  "definitions": {
    "protoVersionResponse": {
      "type": "object",
      "properties": {
        "value": {
          "type": "string"
        }
      }
    }
  }
}
`
)

func TestFlatMapComprehensiveNested(t *testing.T) {
	type args struct {
		data map[string]interface{}
	}
	tests := []struct {
		name string
		args args
		want map[string]interface{}
	}{
		{
			name: "#1: Make sure no error during parse complex JSON",
			args: args{
				data: func() map[string]interface{} {

					arg := make(map[string]interface{})
					_ = json.Unmarshal([]byte(rawJSONTest), &arg)
					return arg
				}(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := make(map[string]interface{})
			builder := func(key string, val interface{}) {
				got[key] = val
			}
			FlatMap("", tt.args.data, builder)
			raw, _ := json.Marshal(got)
			assert.Greater(t, len(string(raw)), 0)
		})
	}
}
