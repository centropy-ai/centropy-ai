package oauthcontext

import (
	"context"
)

type User struct {
	ID       uint32
	Email    string
	TenantID uint32
}

type AuthContext func(ctx context.Context) (User, bool)
