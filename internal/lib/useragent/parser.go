package useragent

import (
	"github.com/ua-parser/uap-go/uaparser"
)

var (
	Parser *uaparser.Parser
)

func init() {
	parser, err := uaparser.New("./regex.yaml")
	if err != nil {
		panic(err)
	}

	Parser = parser
}
