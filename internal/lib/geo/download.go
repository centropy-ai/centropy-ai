package geo

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"net/http"
	"os"

	"gitlab.com/centropy-ai/centropy-ai/log"
	"gitlab.com/centropy-ai/centropy-ai/log/field"
)

func downloadGeoIP(edition, dst string) {
	fileUrl := fmt.Sprintf("https://download.maxmind.com/app/geoip_download?edition_id=%s&license_key=ue2TxBRjJ2IWA65D&suffix=tar.gz", edition)

	if err := downloadFile(fmt.Sprintf("%s.mmdb.tar.gz", edition), fileUrl); err != nil {
		panic(err)
	}
	r, err := os.Open(fmt.Sprintf("%s.mmdb.tar.gz", edition))
	if err != nil {
		fmt.Println("error")
	}
	extractTarGz(r, edition, dst)
}

// DownloadFile will download a url to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory.
func downloadFile(filepath string, url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

func extractTarGz(gzipStream io.Reader, edition, dst string) {
	uncompressedStream, err := gzip.NewReader(gzipStream)
	if err != nil {
		log.Info("ExtractTarGz: NewReader failed")
		return
	}

	tarReader := tar.NewReader(uncompressedStream)
	var extractedDir string
	for true {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Info("ExtractTarGz: Next() failed: ", field.Error(err))
		}
		switch header.Typeflag {
		case tar.TypeDir:
			extractedDir = header.Name
			if err := os.Mkdir(header.Name, 0755); err != nil {
				//log.Fatalf("ExtractTarGz: Mkdir() failed: %s", err.Error())
			}
		case tar.TypeReg:
			outFile, err := os.Create(header.Name)
			if err != nil {
				return
			}
			if _, err := io.Copy(outFile, tarReader); err != nil {
				return
			}
			outFile.Close()

		default:
			log.Error(
				"ExtractTarGz: uknown type: ", field.String("edition", edition))
		}

	}
	copy(fmt.Sprintf("./%s/%s", extractedDir, fmt.Sprintf("%s.mmdb", edition)), dst)
}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}
