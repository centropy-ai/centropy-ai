package geo

import (
	"fmt"
	"net"
	"os"

	geoip2 "github.com/oschwald/geoip2-golang"
	"gitlab.com/centropy-ai/centropy-ai/log"
	"gitlab.com/centropy-ai/centropy-ai/log/field"
)

var (
	connectionType *geoip2.Reader
	isp            *geoip2.Reader
	city           *geoip2.Reader
)

func InitGeoIP() {
	currentDir, _ := os.Getwd()
	reader, err := geoip2.Open(fmt.Sprintf("%s/internal/lib/geo/GeoIP2-Connection-Type.mmdb", currentDir))
	if err != nil {
		panic(err)
	}
	connectionType = reader
	ispReader, errIsp := geoip2.Open(fmt.Sprintf("%s/internal/lib/geo/GeoIP2-ISP.mmdb", currentDir))
	if errIsp != nil {
		panic(errIsp)
	}
	isp = ispReader
	cityReader, errIsp := geoip2.Open(fmt.Sprintf("%s/internal/lib/geo/GeoIP2-City.mmdb", currentDir))
	if errIsp != nil {
		panic(errIsp)
	}
	city = cityReader
}

func GetConnectionTypeIP(ip string) *geoip2.ConnectionType {
	ctype, e := connectionType.ConnectionType(net.ParseIP(ip))
	if e != nil {
		log.Error("not found connection", field.String("ip", ip))
	}
	return ctype
}

func GetMaxmindIsp(ip string) *geoip2.ISP {
	info, e := isp.ISP(net.ParseIP(ip))
	if e != nil {
		panic(e)
	}
	return info
}

func GetCity(ip string) *geoip2.City {
	info, e := city.City(net.ParseIP(ip))
	if e != nil {
		panic(e)
	}
	return info
}

func CombineCity(city *geoip2.City) string {
	return fmt.Sprintf("%d", city.City.GeoNameID)
}
