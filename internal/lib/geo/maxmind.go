package geo

import (
	"fmt"
	"os"
	"sync"

	"github.com/carlescere/scheduler"
)

var (
	loadOnce = new(sync.Once)
)

func init() {
	currentDir, _ := os.Getwd()
	loadOnce.Do(func() {
		scheduler.Every(5).Day().NotImmediately().Run(func() {
			downloadGeoIP("GeoIP2-Connection-Type", fmt.Sprintf("%s/internal/lib/geo/GeoIP2-Connection-Type.mmdb", currentDir))
			downloadGeoIP("GeoIP2-ISP", fmt.Sprintf("%s/internal/lib/geo/GeoIP2-ISP.mmdb", currentDir))
			downloadGeoIP("GeoIP2-City", fmt.Sprintf("%s/internal/lib/geo/GeoIP2-City.mmdb", currentDir))
			InitGeoIP()
		})
	})
}
