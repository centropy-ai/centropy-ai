package env

const (
	KeyConfigStorage   = "CONFIG_STORAGE"
	KeyConfigNamespace = "CONFIG_NAMESPACE"
	KeyConfigMode      = "ENV"
)
