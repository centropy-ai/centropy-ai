package loader

import (
	"net/url"

	"gitlab.com/centropy-ai/centropy-ai/config/loader/consul"
	"gitlab.com/centropy-ai/centropy-ai/config/loader/file"
	"gitlab.com/centropy-ai/centropy-ai/config/namespace"
	"gitlab.com/centropy-ai/centropy-ai/config/storage"
)

type Loader interface {
	Load(namespace namespace.Namespace, value interface{}) error
}

func New(s storage.Storage) Loader {
	storage := s.String()
	url, err := url.ParseRequestURI(storage)
	if err != nil || (url.Host == "" && url.Scheme == "") {
		return file.New(storage)
	}
	return consul.New(storage)
}
