package namespace

import (
	"fmt"
	"os"

	"gitlab.com/centropy-ai/centropy-ai/config/env"
)

const (
	namespaceFmt = "gitlab.com/centropy-ai/centropy-ai/%s/services/%s"
)

type Namespace string

func (n Namespace) String() string {
	return string(n)
}

func FromString(value string) Namespace {
	return Namespace(value)
}

func FromMode(service string) Namespace {
	mode := os.Getenv(env.KeyConfigMode)
	if mode == "" {
		mode = "prod"
	}
	return FromString(fmt.Sprintf(namespaceFmt, mode, service))
}
