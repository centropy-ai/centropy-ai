package handler

import (
	"context"
	"os"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/centropy-ai/centropy-ai/proto"
)

type CentropyAI struct {
}

func (m *CentropyAI) Version(context.Context, *empty.Empty) (*proto.VersionResponse, error) {
	version := os.Getenv("VERSION")
	if version == "" {
		version = "unknown-version"
	}
	return &proto.VersionResponse{Value: version}, nil
}
