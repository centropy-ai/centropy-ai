package dto

type StringAttr struct {
	Key   string
	Value string
}

func (s StringAttr) GetKey() string {
	return s.Key
}

func (s StringAttr) GetValue() interface{} {
	return s.Value
}

type NumericAttr struct {
	Key   string
	Value float64
}

func (n NumericAttr) GetKey() string {
	return n.Key
}

func (n NumericAttr) GetValue() interface{} {
	return n.Value
}

type TrackingProperty interface {
	GetKey() string
	GetValue() interface{}
}
