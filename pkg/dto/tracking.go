package dto

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/centropy-ai/centropy-ai/core/util"
)

type TrackingRequest struct {
	Events    []Event `json:"events,omitempty"`
	Source    Item    `json:"source"`
	SessionID string  `json:"sessionId"`
	TenantID  int32   `json:"tenantId"`
}

type Event struct {
	Item
	EventType string `json:"eventType"`
	SessionID string `json:"sessionId"`
	ProfileID string `json:"profileId"`
	Timestamp string `json:"timeStamp"`
	Source    Item   `json:"source"`
	Target    Item   `json:"target"`
	IP        string `json:"ip,omitempty"`
	UserAgent string `json:"userAgent,omitempty"`
}

func (e *Event) Unmarshal(r []byte) error {
	return json.Unmarshal(r, e)
}

type Item struct {
	ItemId     string                 `json:"itemId,omitempty"`
	ItemType   string                 `json:"itemType,omitempty"`
	Scope      string                 `json:"scope,omitempty"`
	Properties map[string]interface{} `json:"properties"`
}

func (i Item) GetProperties(prefix string) []TrackingProperty {
	var data []TrackingProperty
	util.FlatMap(fmt.Sprintf("%s.properties.", prefix), i.Properties, func(s string, i interface{}) {
		if val, ok := i.(string); ok {
			data = append(data, StringAttr{Key: s, Value: val})
			return
		}
		val, ok := i.(float64)
		if !ok {
			panic(errors.New(fmt.Sprintf("We didn't support property key %s - value: %v", s, i)))
			return
		}
		data = append(data, NumericAttr{Key: s, Value: val})
	})

	return data
}
