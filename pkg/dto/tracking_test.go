package dto

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestItem_GetFlatMap(t *testing.T) {
	type fields struct {
		ItemId     string
		ItemType   string
		Scope      string
		Properties map[string]interface{}
	}
	type args struct {
		prefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []TrackingProperty
	}{
		{
			name: "#1: Basic property",
			fields: fields{
				ItemId:     "i-1",
				ItemType:   "booking",
				Scope:      "ecommerce",
				Properties: map[string]interface{}{"price": 15.2},
			},
			args: args{prefix: "target"},
			want: []TrackingProperty{
				NumericAttr{
					Key:   "target.properties.price",
					Value: 15.2,
				},
			},
		},
		{
			name: "#2: Boolean property",
			fields: fields{
				ItemId:     "i-1",
				ItemType:   "review",
				Scope:      "ecommerce",
				Properties: map[string]interface{}{"like": true, "content": "I like it"},
			},
			args: args{prefix: "scope"},
			want: []TrackingProperty{
				NumericAttr{
					Key:   "scope.properties.like",
					Value: 1,
				},
				StringAttr{
					Key:   "scope.properties.content",
					Value: "I like it",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := Item{
				ItemId:     tt.fields.ItemId,
				ItemType:   tt.fields.ItemType,
				Scope:      tt.fields.Scope,
				Properties: tt.fields.Properties,
			}
			if got := i.GetProperties(tt.args.prefix); !assert.ElementsMatch(t, got, tt.want) {
				t.Errorf("GetProperties() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEvent_Unmarshal(t *testing.T) {
	type args struct {
		r []byte
	}
	tests := []struct {
		name     string
		args     args
		wantErr  bool
		assertMe Event
	}{
		{
			name: "#1: json string",
			args: args{r: func() []byte {
				return []byte(`{"itemId":"i1","itemType":"event","eventType":"booking","sessionId":"s-1","profileId":"p-1"}`)
			}()},
			wantErr:  false,
			assertMe: Event{Item: Item{ItemId: "i1", ItemType: "event"}, EventType: "booking", SessionID: "s-1", ProfileID: "p-1"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &Event{}
			if err := e.Unmarshal(tt.args.r); (err != nil) != tt.wantErr {
				t.Errorf("Unmarshal() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.ObjectsAreEqual(*e, tt.assertMe)
		})
	}
}
