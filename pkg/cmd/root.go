package cmd

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/centropy-ai/centropy-ai/bootstrap"
	cache "gitlab.com/centropy-ai/centropy-ai/cache/arc"
	"gitlab.com/centropy-ai/centropy-ai/cmd"
	"gitlab.com/centropy-ai/centropy-ai/config"
	"gitlab.com/centropy-ai/centropy-ai/config/loader"
	"gitlab.com/centropy-ai/centropy-ai/log"
	"gitlab.com/centropy-ai/centropy-ai/pkg/handler"
	"gitlab.com/centropy-ai/centropy-ai/proto"
	"gitlab.com/centropy-ai/centropy-ai/server"
	"gitlab.com/centropy-ai/centropy-ai/server/grpc"
	"gitlab.com/centropy-ai/centropy-ai/server/http"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	googlegrpc "google.golang.org/grpc"
)

var aceCmd = &cobra.Command{
	Use:   "centropy",
	Short: "Start Centropy MetaRouter Service",
	Long:  "OK babe",
	Run: func(cmd *cobra.Command, args []string) {
		startAce()
	},
}

var (
	onClose func()
)

func init() {
	cmd.Register(aceCmd)

	aceCmd.PersistentFlags().String("namespace", "ace", "config namespace")
	viper.BindPFlag("namespace", aceCmd.PersistentFlags().Lookup("namespace"))
}

func startAce() {
	var (
		s             = server.Server{}
		mr            = new(handler.CentropyAI)
		registerAsset = registerBackend(mr)
		ctx, cancel   = context.WithCancel(context.Background())
	)
	defer cancel()

	var objects = []bootstrap.Object{
		bootstrap.ByValue(&s),
		//centropy
		bootstrap.ByName("centropy", mr),
	}

	if err := bootstrap.Populate(func(loader loader.Loader) []bootstrap.Object {
		cfg := &config.Config{}
		config.LoadWithPlaceholder(loader, cfg)
		bootstrap.LoadDB(cfg.Database)
		bootstrap.LoadGraphDB(cfg.Database)
		grpcCfg, httpCfg := cfg.GRPC, cfg.HTTP
		objects = append(objects,
			bootstrap.ByName("db_cache", cache.New()),
			bootstrap.ByName("grpc", newGRPCServer(grpcServerOption{
				RegisterFuncs: []grpc.RegisterFunc{registerAsset},
				Config:        grpcCfg,
			})),
			bootstrap.ByName("http", newHTTPServer(httpServerOption{
				ConfigGRPC: grpcCfg,
				ConfigHTTP: httpCfg,
			})),
		)
		return objects
	}); err != nil {
		panic(err)
	}

	if err := s.Run(ctx); err != nil {
		panic(err)
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGINT)

	select {
	case s := <-c:
		log.Info(fmt.Sprintf("received signal: %s", s))
	case <-ctx.Done():
		onClose()
		log.Info("onClose received context done")
	}
}

type grpcServerOption struct {
	RegisterFuncs []grpc.RegisterFunc
	Config        *config.GRPC
	JWTSecret     string
}

func newGRPCServer(opt grpcServerOption) *grpc.Server {
	s := grpc.New(
		grpc.WithAddress(opt.Config.Address),
		grpc.WithRegisterFunc(opt.RegisterFuncs...),
	)
	return s
}

type httpServerOption struct {
	ConfigGRPC *config.GRPC
	ConfigHTTP *config.HTTP
}

func newHTTPServer(opt httpServerOption) *http.Server {
	s := http.New(
		http.WithGRPCAddress(opt.ConfigGRPC.Address),
		http.WithAddress(opt.ConfigHTTP.Address),
		http.WithHandlerFunc(proto.RegisterBackendHandlerFromEndpoint),
	)

	return s
}

func registerBackend(mr *handler.CentropyAI) func(*googlegrpc.Server) {
	return func(s *googlegrpc.Server) {
		proto.RegisterBackendServer(s, mr)
	}
}
