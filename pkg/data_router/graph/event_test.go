package graph

import (
	"context"
	"testing"
	"time"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
	cache "gitlab.com/centropy-ai/centropy-ai/cache/arc"
	"gitlab.com/centropy-ai/centropy-ai/ent"
	"gitlab.com/centropy-ai/centropy-ai/ent/event"
	"gitlab.com/centropy-ai/centropy-ai/ent/stringattribute"
	"gitlab.com/centropy-ai/centropy-ai/log"
	"gitlab.com/centropy-ai/centropy-ai/log/field"
	"gitlab.com/centropy-ai/centropy-ai/pkg/dto"
)

func BenchmarkHandle(t *testing.B) {
	mdb, err := ent.Open("sqlite3", "file::memory:?cache=private&_fk=1")
	if err != nil {
		t.Fatal(err)
	}
	if err := mdb.Schema.Create(context.Background()); err != nil {
		t.Fatal(err)
	}
	w := NewWithGraphClient()
	w.Client = mdb
	data := dto.TrackingRequest{
		Events: []dto.Event{
			{
				Item:      dto.Item{},
				Timestamp: time.Now().Format(time.RFC822),
				Source: dto.Item{
					ItemId:   "ecommerce",
					ItemType: "crm",
					Scope:    "ecommerce",
				},
				Target: dto.Item{
					ItemId:   "/book/learning",
					ItemType: "book",
					Scope:    "ecommerce",
					Properties: map[string]interface{}{
						"price": 12,
						"name":  "Learning Programming",
					},
				},
			},
		},
		Source: dto.Item{
			ItemId:   "ecommerce",
			ItemType: "crm",
			Scope:    "ecommerce",
		},
		SessionID: "session-id",
	}
	log.Info("data", field.Any("data", data))
	for i := 0; i < t.N; i++ {
		w.Save(context.Background(), data)
	}
}

//func Test_writer_createProperties(t *testing.T) {
//	mdb, err := ent.Open("sqlite3", "file::memory:?cache=private&_fk=1")
//	if err != nil {
//		t.Fatal(err)
//	}
//	if err := mdb.Schema.Create(context.Background()); err != nil {
//		t.Fatal(err)
//	}
//	type fields struct {
//		Client *ent.Client
//		Arc    cache.Cache
//	}
//	type args struct {
//		source entity.Item
//		target entity.Item
//	}
//	tests := []struct {
//		name   string
//		fields fields
//		args   args
//		want   []*ent.NumericAttribute
//		want1  []*ent.StringAttribute
//	}{
//		{
//			name: "#1: Simple",
//			args: args{
//				source: entity.Item{
//					ItemId:     "paul",
//					ItemType:   "human",
//					Scope:      "family",
//					Properties: map[string]interface{}{"me": "Paul", "age": 34, "family": map[string]interface{}{"brother": 2}},
//				},
//				target: entity.Item{
//					ItemId:     "stella",
//					ItemType:   "human",
//					Scope:      "family",
//					Properties: map[string]interface{}{"wife": "Stella", "age": 30, "pageInfo": map[string]interface{}{"path": "/root"}},
//				},
//			},
//			want: []*ent.NumericAttribute{
//				{
//					Key:   "source.properties.age",
//					Value: 34,
//				},
//				{
//					Key:   "source.properties.family.brother",
//					Value: 2,
//				},
//				{
//					Key:   "target.properties.age",
//					Value: 30,
//				},
//			},
//			want1: []*ent.StringAttribute{
//				{
//					Key:   "source.itemId",
//					Value: "paul",
//				},
//				{
//					Key:   "source.itemType",
//					Value: "human",
//				},
//				{
//					Key:   "source.scope",
//					Value: "family",
//				},
//				{
//					Key:   "source.properties.me",
//					Value: "Paul",
//				},
//				{
//					Key:   "target.itemId",
//					Value: "stella",
//				},
//				{
//					Key:   "target.itemType",
//					Value: "human",
//				},
//				{
//					Key:   "target.scope",
//					Value: "family",
//				},
//				{
//					Key:   "target.properties.wife",
//					Value: "Stella",
//				},
//				{
//					Key:   "target.properties.pageInfo.path",
//					Value: "/root",
//				},
//			},
//		},
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			ctx := context.Background()
//			tx, _ := mdb.Tx(ctx)
//			w := writer{
//				Client: tt.fields.Client,
//				Arc:    tt.fields.Arc,
//			}
//			got, got1 := w.createProperties(tx, ctx, tt.args.source, tt.args.target)
//			if !assert.ElementsMatch(t, got, tt.want) {
//				t.Errorf("createProperties() got = %v, want %v", got, tt.want)
//			}
//			if !assert.ElementsMatch(t, got1, tt.want1) {
//				t.Errorf("createProperties() got1 = %v, want %v", got1, tt.want1)
//			}
//		})
//	}
//}

func Test_writer_Save(t *testing.T) {
	type fields struct {
		Client *ent.Client
		Arc    cache.Cache
	}
	type args struct {
		ctx  context.Context
		data dto.TrackingRequest
	}
	tests := []struct {
		name         string
		fields       fields
		args         args
		wantErr      bool
		assertClient func(client *ent.Client, ctx context.Context) bool
	}{
		{
			name: "#1: Write data: expect 1 string row, 1 numeric row and 1 event",
			fields: fields{
				Client: func() *ent.Client {
					mdb, err := ent.Open("sqlite3", "file::memory:?cache=private&_fk=1")
					if err != nil {
						t.Fatal(err)
					}
					if err := mdb.Schema.Create(context.Background()); err != nil {
						t.Fatal(err)
					}
					return mdb
				}(),
				Arc: cache.New(),
			},
			args: args{
				ctx: context.Background(),
				data: dto.TrackingRequest{
					Events: []dto.Event{
						{
							Item: dto.Item{
								ItemId:   "event-1",
								ItemType: "event",
								Scope:    "ecom",
							},
							EventType: "booking",
							SessionID: "s-1",
							ProfileID: "p-1",
							Timestamp: time.Now().Format(time.RFC822),
							Source: dto.Item{
								ItemId:     "s-1",
								ItemType:   "site",
								Scope:      "ecom",
								Properties: map[string]interface{}{"ecommerce": true},
							},
							Target: dto.Item{
								ItemId:     "t-1",
								ItemType:   "book",
								Scope:      "ecom",
								Properties: map[string]interface{}{"name": "EASY for mom"},
							},
						},
					},
					Source: dto.Item{
						ItemId:   "ecom",
						ItemType: "site",
						Scope:    "ecom",
					},
					SessionID: "s-1",
				},
			},
			wantErr: false,
			assertClient: func(cl *ent.Client, ctx context.Context) bool {
				nAs, _ := cl.NumericAttribute.Query().All(ctx)
				assert.Equal(t, 1, len(nAs))
				assert.Equal(t, "source.properties.ecommerce", nAs[0].Key)
				assert.Equal(t, 1.0, nAs[0].Value)
				sAs, _ := cl.StringAttribute.Query().All(ctx)
				assert.Equal(t, 1, len(sAs))
				assert.Equal(t, "target.properties.name", sAs[0].Key)
				assert.Equal(t, "EASY for mom", sAs[0].Value)
				return true
			},
		},
		{
			name: "#1.1: Write data: expect 1 string row, 2 numeric row and 1 event",
			fields: fields{
				Client: func() *ent.Client {
					mdb, err := ent.Open("sqlite3", "file::memory:?cache=private&_fk=1")
					if err != nil {
						t.Fatal(err)
					}
					if err := mdb.Schema.Create(context.Background()); err != nil {
						t.Fatal(err)
					}
					return mdb
				}(),
				Arc: cache.New(),
			},
			args: args{
				ctx: context.Background(),
				data: dto.TrackingRequest{
					Events: []dto.Event{
						{
							Item: dto.Item{
								ItemId:   "event-1",
								ItemType: "event",
								Scope:    "ecom",
							},
							EventType: "booking",
							SessionID: "s-1",
							ProfileID: "p-1",
							Timestamp: time.Now().Format(time.RFC822),
							Source: dto.Item{
								ItemId:     "s-1",
								ItemType:   "site",
								Scope:      "ecom",
								Properties: map[string]interface{}{"ecommerce": true},
							},
							Target: dto.Item{
								ItemId:     "t-1",
								ItemType:   "book",
								Scope:      "ecom",
								Properties: map[string]interface{}{"name": "EASY for mom", "price": 12.5},
							},
						},
					},
					Source: dto.Item{
						ItemId:   "ecom",
						ItemType: "site",
						Scope:    "ecom",
					},
					SessionID: "s-1",
				},
			},
			wantErr: false,
			assertClient: func(cl *ent.Client, ctx context.Context) bool {
				nAs, _ := cl.NumericAttribute.Query().All(ctx)
				assert.Equal(t, 2, len(nAs))
				assert.Equal(t, "source.properties.ecommerce", nAs[0].Key)
				assert.Equal(t, "target.properties.price", nAs[1].Key)
				assert.Equal(t, 1.0, nAs[0].Value)
				assert.Equal(t, 12.5, nAs[1].Value)
				sAs, _ := cl.StringAttribute.Query().All(ctx)
				assert.Equal(t, 1, len(sAs))
				assert.Equal(t, "target.properties.name", sAs[0].Key)
				assert.Equal(t, "EASY for mom", sAs[0].Value)
				return true
			},
		},
		{
			name: "#2: Write 2 events",
			fields: fields{
				Client: func() *ent.Client {
					mdb, err := ent.Open("sqlite3", "file::memory:?cache=private&_fk=1")
					if err != nil {
						t.Fatal(err)
					}
					if err := mdb.Schema.Create(context.Background()); err != nil {
						t.Fatal(err)
					}
					return mdb
				}(),
				Arc: cache.New(),
			},
			args: args{
				ctx: context.Background(),
				data: dto.TrackingRequest{
					Events: []dto.Event{
						{
							Item: dto.Item{
								ItemId:   "event-1",
								ItemType: "event",
								Scope:    "ecom",
							},
							EventType: "booking",
							SessionID: "s-1",
							ProfileID: "p-1",
							Timestamp: time.Now().Format(time.RFC822),
							Source: dto.Item{
								ItemId:     "s-1",
								ItemType:   "site",
								Scope:      "ecom",
								Properties: map[string]interface{}{"ecommerce": true},
							},
							Target: dto.Item{
								ItemId:     "t-1",
								ItemType:   "book",
								Scope:      "ecom",
								Properties: map[string]interface{}{"name": "EASY for mom", "price": 12.5},
							},
						},
						{
							Item: dto.Item{
								ItemId:   "event-1",
								ItemType: "event",
								Scope:    "ecom",
							},
							EventType: "booking",
							SessionID: "s-2",
							ProfileID: "p-1",
							Timestamp: time.Now().Format(time.RFC822),
							Source: dto.Item{
								ItemId:     "s-2",
								ItemType:   "site",
								Scope:      "ecom",
								Properties: map[string]interface{}{"ecommerce": true},
							},
							Target: dto.Item{
								ItemId:     "t-2",
								ItemType:   "book",
								Scope:      "ecom",
								Properties: map[string]interface{}{"name": "EASY for mom", "price": 12.5},
							},
						},
					},
					Source: dto.Item{
						ItemId:   "ecom",
						ItemType: "site",
						Scope:    "ecom",
					},
					SessionID: "s-1",
				},
			},
			wantErr: false,
			assertClient: func(cl *ent.Client, ctx context.Context) bool {
				nAs, _ := cl.NumericAttribute.Query().All(ctx)
				assert.Equal(t, 4, len(nAs))
				sAs, _ := cl.StringAttribute.Query().Order(ent.Asc(stringattribute.FieldID)).All(ctx)
				assert.Equal(t, 2, len(sAs))
				assert.Equal(t, "target.properties.name", sAs[0].Key)
				assert.Equal(t, "target.properties.name", sAs[1].Key)
				assert.Equal(t, "t-1", sAs[0].ItemId)
				assert.Equal(t, "t-2", sAs[1].ItemId)

				evs, _ := cl.Event.Query().Order(ent.Asc(event.FieldID)).All(ctx)
				assert.Equal(t, 2, len(evs), "Total event records is 2")
				assert.Equal(t, "s-1", evs[0].SessionID, "First event belong to session 1")
				assert.Equal(t, "s-2", evs[1].SessionID, "Last event is session 2")
				return true
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := writer{
				Client: tt.fields.Client,
				Arc:    tt.fields.Arc,
			}
			if err := w.Save(tt.args.ctx, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
			tt.assertClient(w.Client, tt.args.ctx)
			defer w.Client.Close()
		})
	}
}
