package graph

import (
	"context"
	"strings"
	"time"

	cache "gitlab.com/centropy-ai/centropy-ai/cache/arc"
	"gitlab.com/centropy-ai/centropy-ai/ent"
	"gitlab.com/centropy-ai/centropy-ai/ent/user"
	"gitlab.com/centropy-ai/centropy-ai/log"
	"gitlab.com/centropy-ai/centropy-ai/log/field"
	"gitlab.com/centropy-ai/centropy-ai/pkg/dto"
)

type writer struct {
	Client *ent.Client `inject:"graph_client"`
	Arc    cache.Cache `inject:"db_cache"`
}

func NewWithGraphClient() *writer {
	return &writer{}
}

func (w writer) Save(ctx context.Context, data dto.TrackingRequest) error {
	for _, event := range data.Events {
		if event.ItemType != "event" {
			continue
		}
		p, err := w.profileBy(ctx, event.ProfileID)
		if err != nil {
			createUser := w.Client.User.Create().SetUserID(event.ProfileID)
			email, ok := event.Properties["email"].(string)
			if len(email) > 0 && ok {
				createUser.SetEmail([]string{email})
			}
			createUser.SetSessionIds([]string{event.SessionID})
			p, err = createUser.Save(ctx)
			if err != nil {
				log.Error("can not save user", field.Error(err))
				panic(err)
			}
		} else {
			if !strings.Contains(strings.Join(p.SessionIds, ","), event.SessionID) {
				sIDs := append(p.SessionIds, event.SessionID)
				_, err := p.Update().SetSessionIds(sIDs).Save(ctx)
				if err != nil {
					log.Error("can not add session id", field.Error(err))
					panic(err)
				}
			}
		}
		_at, _ := time.Parse(time.RFC822, event.Timestamp)
		tx, err := w.Client.Tx(ctx)
		if err != nil {
			panic(err)
		}
		nAs, sAs := w.createProperties(tx, ctx, event.Source, event.Target)
		_, err = tx.Event.Create().
			SetTenant(int8(data.TenantID)).
			SetName(event.EventType).
			SetScope(event.Scope).
			SetItemId(event.ItemId).
			SetSessionID(event.SessionID).
			AddNumAttr(nAs...).AddStrAttr(sAs...).
			SetAt(_at).
			SetBy(p).Save(ctx)
		if err != nil {
			_ = tx.Rollback()
			log.Error("can not commit request", field.Any("request", event), field.Any("request", data), field.Error(err))
			panic(err)
		}
		_ = tx.Commit()
	}
	return nil
}

func (w writer) profileBy(ctx context.Context, ID string) (*ent.User, error) {
	if u, ok := w.Arc.Get(ID); ok {
		res, ok := u.(*ent.User)
		if ok {
			return res, nil
		}
	}
	us, err := w.Client.User.Query().Where(user.UserID(ID)).First(ctx)
	if err != nil {
		return nil, err
	}

	w.Arc.Add(ID, us)
	return us, nil
}

func (w writer) createProperties(tx *ent.Tx, ctx context.Context, source dto.Item, target dto.Item) ([]*ent.NumericAttribute, []*ent.StringAttribute) {
	sources := source.GetProperties("source")
	targets := target.GetProperties("target")
	sizeSource := len(sources) + len(targets)
	numAttr := make([]*ent.NumericAttribute, 0, sizeSource)
	strAttr := make([]*ent.StringAttribute, 0, sizeSource)
	numAttr, strAttr = transformProps(tx, ctx, source, sources, numAttr, strAttr)
	numAttr, strAttr = transformProps(tx, ctx, target, targets, numAttr, strAttr)
	return numAttr, strAttr
}

func transformProps(tx *ent.Tx, ctx context.Context, item dto.Item, props []dto.TrackingProperty, nA []*ent.NumericAttribute, sA []*ent.StringAttribute) ([]*ent.NumericAttribute, []*ent.StringAttribute) {
	for _, v := range props {
		switch val := v.(type) {
		case dto.StringAttr:
			e := tx.Client().StringAttribute.Create().SetItemId(item.ItemId).SetItemType(item.ItemType).
				SetValue(val.Value).SetKey(val.Key).SaveX(ctx)
			sA = append(sA, e)
		case dto.NumericAttr:
			e := tx.Client().NumericAttribute.Create().SetItemId(item.ItemId).SetItemType(item.ItemType).
				SetValue(val.Value).SetKey(val.Key).SaveX(ctx)
			nA = append(nA, e)
		}
	}
	return nA, sA
}
