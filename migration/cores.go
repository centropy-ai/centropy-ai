package migration

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func CoreMigrations() []*gormigrate.Migration {
	return []*gormigrate.Migration{
		{
			ID: "schema-database",
			Migrate: func(db *gorm.DB) error {
				return db.AutoMigrate().Error
			},
		},
	}
}
