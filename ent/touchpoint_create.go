// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"encoding/json"
	"errors"

	"github.com/facebookincubator/ent/dialect/sql/sqlgraph"
	"github.com/facebookincubator/ent/schema/field"
	"gitlab.com/centropy-ai/centropy-ai/ent/campaign"
	"gitlab.com/centropy-ai/centropy-ai/ent/lead"
	"gitlab.com/centropy-ai/centropy-ai/ent/pointtrigger"
	"gitlab.com/centropy-ai/centropy-ai/ent/touchpoint"
)

// TouchPointCreate is the builder for creating a TouchPoint entity.
type TouchPointCreate struct {
	config
	event            *string
	position         *int
	optional         *bool
	duration_minutes *uint
	condition        *json.RawMessage
	triggers         map[int]struct{}
	leads            map[int]struct{}
	yes              map[int]struct{}
	no               map[int]struct{}
	campaign         map[int]struct{}
}

// SetEvent sets the event field.
func (tpc *TouchPointCreate) SetEvent(s string) *TouchPointCreate {
	tpc.event = &s
	return tpc
}

// SetPosition sets the position field.
func (tpc *TouchPointCreate) SetPosition(i int) *TouchPointCreate {
	tpc.position = &i
	return tpc
}

// SetNillablePosition sets the position field if the given value is not nil.
func (tpc *TouchPointCreate) SetNillablePosition(i *int) *TouchPointCreate {
	if i != nil {
		tpc.SetPosition(*i)
	}
	return tpc
}

// SetOptional sets the optional field.
func (tpc *TouchPointCreate) SetOptional(b bool) *TouchPointCreate {
	tpc.optional = &b
	return tpc
}

// SetNillableOptional sets the optional field if the given value is not nil.
func (tpc *TouchPointCreate) SetNillableOptional(b *bool) *TouchPointCreate {
	if b != nil {
		tpc.SetOptional(*b)
	}
	return tpc
}

// SetDurationMinutes sets the duration_minutes field.
func (tpc *TouchPointCreate) SetDurationMinutes(u uint) *TouchPointCreate {
	tpc.duration_minutes = &u
	return tpc
}

// SetNillableDurationMinutes sets the duration_minutes field if the given value is not nil.
func (tpc *TouchPointCreate) SetNillableDurationMinutes(u *uint) *TouchPointCreate {
	if u != nil {
		tpc.SetDurationMinutes(*u)
	}
	return tpc
}

// SetCondition sets the condition field.
func (tpc *TouchPointCreate) SetCondition(jm json.RawMessage) *TouchPointCreate {
	tpc.condition = &jm
	return tpc
}

// AddTriggerIDs adds the triggers edge to PointTrigger by ids.
func (tpc *TouchPointCreate) AddTriggerIDs(ids ...int) *TouchPointCreate {
	if tpc.triggers == nil {
		tpc.triggers = make(map[int]struct{})
	}
	for i := range ids {
		tpc.triggers[ids[i]] = struct{}{}
	}
	return tpc
}

// AddTriggers adds the triggers edges to PointTrigger.
func (tpc *TouchPointCreate) AddTriggers(p ...*PointTrigger) *TouchPointCreate {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return tpc.AddTriggerIDs(ids...)
}

// AddLeadIDs adds the leads edge to Lead by ids.
func (tpc *TouchPointCreate) AddLeadIDs(ids ...int) *TouchPointCreate {
	if tpc.leads == nil {
		tpc.leads = make(map[int]struct{})
	}
	for i := range ids {
		tpc.leads[ids[i]] = struct{}{}
	}
	return tpc
}

// AddLeads adds the leads edges to Lead.
func (tpc *TouchPointCreate) AddLeads(l ...*Lead) *TouchPointCreate {
	ids := make([]int, len(l))
	for i := range l {
		ids[i] = l[i].ID
	}
	return tpc.AddLeadIDs(ids...)
}

// SetYesID sets the yes edge to TouchPoint by id.
func (tpc *TouchPointCreate) SetYesID(id int) *TouchPointCreate {
	if tpc.yes == nil {
		tpc.yes = make(map[int]struct{})
	}
	tpc.yes[id] = struct{}{}
	return tpc
}

// SetNillableYesID sets the yes edge to TouchPoint by id if the given value is not nil.
func (tpc *TouchPointCreate) SetNillableYesID(id *int) *TouchPointCreate {
	if id != nil {
		tpc = tpc.SetYesID(*id)
	}
	return tpc
}

// SetYes sets the yes edge to TouchPoint.
func (tpc *TouchPointCreate) SetYes(t *TouchPoint) *TouchPointCreate {
	return tpc.SetYesID(t.ID)
}

// SetNoID sets the no edge to TouchPoint by id.
func (tpc *TouchPointCreate) SetNoID(id int) *TouchPointCreate {
	if tpc.no == nil {
		tpc.no = make(map[int]struct{})
	}
	tpc.no[id] = struct{}{}
	return tpc
}

// SetNillableNoID sets the no edge to TouchPoint by id if the given value is not nil.
func (tpc *TouchPointCreate) SetNillableNoID(id *int) *TouchPointCreate {
	if id != nil {
		tpc = tpc.SetNoID(*id)
	}
	return tpc
}

// SetNo sets the no edge to TouchPoint.
func (tpc *TouchPointCreate) SetNo(t *TouchPoint) *TouchPointCreate {
	return tpc.SetNoID(t.ID)
}

// SetCampaignID sets the campaign edge to Campaign by id.
func (tpc *TouchPointCreate) SetCampaignID(id int) *TouchPointCreate {
	if tpc.campaign == nil {
		tpc.campaign = make(map[int]struct{})
	}
	tpc.campaign[id] = struct{}{}
	return tpc
}

// SetNillableCampaignID sets the campaign edge to Campaign by id if the given value is not nil.
func (tpc *TouchPointCreate) SetNillableCampaignID(id *int) *TouchPointCreate {
	if id != nil {
		tpc = tpc.SetCampaignID(*id)
	}
	return tpc
}

// SetCampaign sets the campaign edge to Campaign.
func (tpc *TouchPointCreate) SetCampaign(c *Campaign) *TouchPointCreate {
	return tpc.SetCampaignID(c.ID)
}

// Save creates the TouchPoint in the database.
func (tpc *TouchPointCreate) Save(ctx context.Context) (*TouchPoint, error) {
	if tpc.event == nil {
		return nil, errors.New("ent: missing required field \"event\"")
	}
	if tpc.position == nil {
		v := touchpoint.DefaultPosition
		tpc.position = &v
	}
	if tpc.optional == nil {
		v := touchpoint.DefaultOptional
		tpc.optional = &v
	}
	if tpc.duration_minutes == nil {
		v := touchpoint.DefaultDurationMinutes
		tpc.duration_minutes = &v
	}
	if tpc.condition == nil {
		return nil, errors.New("ent: missing required field \"condition\"")
	}
	if len(tpc.yes) > 1 {
		return nil, errors.New("ent: multiple assignments on a unique edge \"yes\"")
	}
	if len(tpc.no) > 1 {
		return nil, errors.New("ent: multiple assignments on a unique edge \"no\"")
	}
	if len(tpc.campaign) > 1 {
		return nil, errors.New("ent: multiple assignments on a unique edge \"campaign\"")
	}
	return tpc.sqlSave(ctx)
}

// SaveX calls Save and panics if Save returns an error.
func (tpc *TouchPointCreate) SaveX(ctx context.Context) *TouchPoint {
	v, err := tpc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (tpc *TouchPointCreate) sqlSave(ctx context.Context) (*TouchPoint, error) {
	var (
		tp    = &TouchPoint{config: tpc.config}
		_spec = &sqlgraph.CreateSpec{
			Table: touchpoint.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: touchpoint.FieldID,
			},
		}
	)
	if value := tpc.event; value != nil {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  *value,
			Column: touchpoint.FieldEvent,
		})
		tp.Event = *value
	}
	if value := tpc.position; value != nil {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeInt,
			Value:  *value,
			Column: touchpoint.FieldPosition,
		})
		tp.Position = *value
	}
	if value := tpc.optional; value != nil {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeBool,
			Value:  *value,
			Column: touchpoint.FieldOptional,
		})
		tp.Optional = *value
	}
	if value := tpc.duration_minutes; value != nil {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeUint,
			Value:  *value,
			Column: touchpoint.FieldDurationMinutes,
		})
		tp.DurationMinutes = *value
	}
	if value := tpc.condition; value != nil {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeJSON,
			Value:  *value,
			Column: touchpoint.FieldCondition,
		})
		tp.Condition = *value
	}
	if nodes := tpc.triggers; len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   touchpoint.TriggersTable,
			Columns: []string{touchpoint.TriggersColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pointtrigger.FieldID,
				},
			},
		}
		for k, _ := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	if nodes := tpc.leads; len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   touchpoint.LeadsTable,
			Columns: []string{touchpoint.LeadsColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: lead.FieldID,
				},
			},
		}
		for k, _ := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	if nodes := tpc.yes; len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2O,
			Inverse: false,
			Table:   touchpoint.YesTable,
			Columns: []string{touchpoint.YesColumn},
			Bidi:    true,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: touchpoint.FieldID,
				},
			},
		}
		for k, _ := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	if nodes := tpc.no; len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2O,
			Inverse: false,
			Table:   touchpoint.NoTable,
			Columns: []string{touchpoint.NoColumn},
			Bidi:    true,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: touchpoint.FieldID,
				},
			},
		}
		for k, _ := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	if nodes := tpc.campaign; len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   touchpoint.CampaignTable,
			Columns: []string{touchpoint.CampaignColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: campaign.FieldID,
				},
			},
		}
		for k, _ := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	if err := sqlgraph.CreateNode(ctx, tpc.driver, _spec); err != nil {
		if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return nil, err
	}
	id := _spec.ID.Value.(int64)
	tp.ID = int(id)
	return tp, nil
}
