// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/facebookincubator/ent/dialect/sql/sqlgraph"
	"github.com/facebookincubator/ent/schema/field"
	"gitlab.com/centropy-ai/centropy-ai/ent/campaign"
	"gitlab.com/centropy-ai/centropy-ai/ent/lead"
	"gitlab.com/centropy-ai/centropy-ai/ent/touchpoint"
)

// CampaignCreate is the builder for creating a Campaign entity.
type CampaignCreate struct {
	config
	tenant       *int8
	name         *string
	description  *string
	release_at   *time.Time
	touch_points map[int]struct{}
	leads        map[int]struct{}
}

// SetTenant sets the tenant field.
func (cc *CampaignCreate) SetTenant(i int8) *CampaignCreate {
	cc.tenant = &i
	return cc
}

// SetName sets the name field.
func (cc *CampaignCreate) SetName(s string) *CampaignCreate {
	cc.name = &s
	return cc
}

// SetDescription sets the description field.
func (cc *CampaignCreate) SetDescription(s string) *CampaignCreate {
	cc.description = &s
	return cc
}

// SetReleaseAt sets the release_at field.
func (cc *CampaignCreate) SetReleaseAt(t time.Time) *CampaignCreate {
	cc.release_at = &t
	return cc
}

// SetNillableReleaseAt sets the release_at field if the given value is not nil.
func (cc *CampaignCreate) SetNillableReleaseAt(t *time.Time) *CampaignCreate {
	if t != nil {
		cc.SetReleaseAt(*t)
	}
	return cc
}

// AddTouchPointIDs adds the touch_points edge to TouchPoint by ids.
func (cc *CampaignCreate) AddTouchPointIDs(ids ...int) *CampaignCreate {
	if cc.touch_points == nil {
		cc.touch_points = make(map[int]struct{})
	}
	for i := range ids {
		cc.touch_points[ids[i]] = struct{}{}
	}
	return cc
}

// AddTouchPoints adds the touch_points edges to TouchPoint.
func (cc *CampaignCreate) AddTouchPoints(t ...*TouchPoint) *CampaignCreate {
	ids := make([]int, len(t))
	for i := range t {
		ids[i] = t[i].ID
	}
	return cc.AddTouchPointIDs(ids...)
}

// AddLeadIDs adds the leads edge to Lead by ids.
func (cc *CampaignCreate) AddLeadIDs(ids ...int) *CampaignCreate {
	if cc.leads == nil {
		cc.leads = make(map[int]struct{})
	}
	for i := range ids {
		cc.leads[ids[i]] = struct{}{}
	}
	return cc
}

// AddLeads adds the leads edges to Lead.
func (cc *CampaignCreate) AddLeads(l ...*Lead) *CampaignCreate {
	ids := make([]int, len(l))
	for i := range l {
		ids[i] = l[i].ID
	}
	return cc.AddLeadIDs(ids...)
}

// Save creates the Campaign in the database.
func (cc *CampaignCreate) Save(ctx context.Context) (*Campaign, error) {
	if cc.tenant == nil {
		return nil, errors.New("ent: missing required field \"tenant\"")
	}
	if cc.name == nil {
		return nil, errors.New("ent: missing required field \"name\"")
	}
	if err := campaign.NameValidator(*cc.name); err != nil {
		return nil, fmt.Errorf("ent: validator failed for field \"name\": %v", err)
	}
	if cc.description == nil {
		return nil, errors.New("ent: missing required field \"description\"")
	}
	if err := campaign.DescriptionValidator(*cc.description); err != nil {
		return nil, fmt.Errorf("ent: validator failed for field \"description\": %v", err)
	}
	if cc.release_at == nil {
		v := campaign.DefaultReleaseAt()
		cc.release_at = &v
	}
	return cc.sqlSave(ctx)
}

// SaveX calls Save and panics if Save returns an error.
func (cc *CampaignCreate) SaveX(ctx context.Context) *Campaign {
	v, err := cc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (cc *CampaignCreate) sqlSave(ctx context.Context) (*Campaign, error) {
	var (
		c     = &Campaign{config: cc.config}
		_spec = &sqlgraph.CreateSpec{
			Table: campaign.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: campaign.FieldID,
			},
		}
	)
	if value := cc.tenant; value != nil {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeInt8,
			Value:  *value,
			Column: campaign.FieldTenant,
		})
		c.Tenant = *value
	}
	if value := cc.name; value != nil {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  *value,
			Column: campaign.FieldName,
		})
		c.Name = *value
	}
	if value := cc.description; value != nil {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  *value,
			Column: campaign.FieldDescription,
		})
		c.Description = *value
	}
	if value := cc.release_at; value != nil {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  *value,
			Column: campaign.FieldReleaseAt,
		})
		c.ReleaseAt = *value
	}
	if nodes := cc.touch_points; len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   campaign.TouchPointsTable,
			Columns: []string{campaign.TouchPointsColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: touchpoint.FieldID,
				},
			},
		}
		for k, _ := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	if nodes := cc.leads; len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   campaign.LeadsTable,
			Columns: []string{campaign.LeadsColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: lead.FieldID,
				},
			},
		}
		for k, _ := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	if err := sqlgraph.CreateNode(ctx, cc.driver, _spec); err != nil {
		if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return nil, err
	}
	id := _spec.ID.Value.(int64)
	c.ID = int(id)
	return c, nil
}
