// Code generated by entc, DO NOT EDIT.

package ent

import (
	"fmt"
	"strings"
	"time"

	"github.com/facebookincubator/ent/dialect/sql"
	"gitlab.com/centropy-ai/centropy-ai/ent/campaign"
)

// Campaign is the model entity for the Campaign schema.
type Campaign struct {
	config `json:"-"`
	// ID of the ent.
	ID int `json:"id,omitempty"`
	// Tenant holds the value of the "tenant" field.
	Tenant int8 `json:"tenant,omitempty"`
	// Name holds the value of the "name" field.
	Name string `json:"name,omitempty"`
	// Description holds the value of the "description" field.
	Description string `json:"description,omitempty"`
	// ReleaseAt holds the value of the "release_at" field.
	ReleaseAt time.Time `json:"release_at,omitempty"`
	// Edges holds the relations/edges for other nodes in the graph.
	// The values are being populated by the CampaignQuery when eager-loading is set.
	Edges CampaignEdges `json:"edges"`
}

// CampaignEdges holds the relations/edges for other nodes in the graph.
type CampaignEdges struct {
	// TouchPoints holds the value of the touch_points edge.
	TouchPoints []*TouchPoint
	// Leads holds the value of the leads edge.
	Leads []*Lead
	// loadedTypes holds the information for reporting if a
	// type was loaded (or requested) in eager-loading or not.
	loadedTypes [2]bool
}

// TouchPointsOrErr returns the TouchPoints value or an error if the edge
// was not loaded in eager-loading.
func (e CampaignEdges) TouchPointsOrErr() ([]*TouchPoint, error) {
	if e.loadedTypes[0] {
		return e.TouchPoints, nil
	}
	return nil, &NotLoadedError{edge: "touch_points"}
}

// LeadsOrErr returns the Leads value or an error if the edge
// was not loaded in eager-loading.
func (e CampaignEdges) LeadsOrErr() ([]*Lead, error) {
	if e.loadedTypes[1] {
		return e.Leads, nil
	}
	return nil, &NotLoadedError{edge: "leads"}
}

// scanValues returns the types for scanning values from sql.Rows.
func (*Campaign) scanValues() []interface{} {
	return []interface{}{
		&sql.NullInt64{},  // id
		&sql.NullInt64{},  // tenant
		&sql.NullString{}, // name
		&sql.NullString{}, // description
		&sql.NullTime{},   // release_at
	}
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the Campaign fields.
func (c *Campaign) assignValues(values ...interface{}) error {
	if m, n := len(values), len(campaign.Columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	value, ok := values[0].(*sql.NullInt64)
	if !ok {
		return fmt.Errorf("unexpected type %T for field id", value)
	}
	c.ID = int(value.Int64)
	values = values[1:]
	if value, ok := values[0].(*sql.NullInt64); !ok {
		return fmt.Errorf("unexpected type %T for field tenant", values[0])
	} else if value.Valid {
		c.Tenant = int8(value.Int64)
	}
	if value, ok := values[1].(*sql.NullString); !ok {
		return fmt.Errorf("unexpected type %T for field name", values[1])
	} else if value.Valid {
		c.Name = value.String
	}
	if value, ok := values[2].(*sql.NullString); !ok {
		return fmt.Errorf("unexpected type %T for field description", values[2])
	} else if value.Valid {
		c.Description = value.String
	}
	if value, ok := values[3].(*sql.NullTime); !ok {
		return fmt.Errorf("unexpected type %T for field release_at", values[3])
	} else if value.Valid {
		c.ReleaseAt = value.Time
	}
	return nil
}

// QueryTouchPoints queries the touch_points edge of the Campaign.
func (c *Campaign) QueryTouchPoints() *TouchPointQuery {
	return (&CampaignClient{c.config}).QueryTouchPoints(c)
}

// QueryLeads queries the leads edge of the Campaign.
func (c *Campaign) QueryLeads() *LeadQuery {
	return (&CampaignClient{c.config}).QueryLeads(c)
}

// Update returns a builder for updating this Campaign.
// Note that, you need to call Campaign.Unwrap() before calling this method, if this Campaign
// was returned from a transaction, and the transaction was committed or rolled back.
func (c *Campaign) Update() *CampaignUpdateOne {
	return (&CampaignClient{c.config}).UpdateOne(c)
}

// Unwrap unwraps the entity that was returned from a transaction after it was closed,
// so that all next queries will be executed through the driver which created the transaction.
func (c *Campaign) Unwrap() *Campaign {
	tx, ok := c.config.driver.(*txDriver)
	if !ok {
		panic("ent: Campaign is not a transactional entity")
	}
	c.config.driver = tx.drv
	return c
}

// String implements the fmt.Stringer.
func (c *Campaign) String() string {
	var builder strings.Builder
	builder.WriteString("Campaign(")
	builder.WriteString(fmt.Sprintf("id=%v", c.ID))
	builder.WriteString(", tenant=")
	builder.WriteString(fmt.Sprintf("%v", c.Tenant))
	builder.WriteString(", name=")
	builder.WriteString(c.Name)
	builder.WriteString(", description=")
	builder.WriteString(c.Description)
	builder.WriteString(", release_at=")
	builder.WriteString(c.ReleaseAt.Format(time.ANSIC))
	builder.WriteByte(')')
	return builder.String()
}

// Campaigns is a parsable slice of Campaign.
type Campaigns []*Campaign

func (c Campaigns) config(cfg config) {
	for _i := range c {
		c[_i].config = cfg
	}
}
