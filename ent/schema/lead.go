package schema

import (
	"time"

	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// Lead holds the schema definition for the Lead entity.
type Lead struct {
	ent.Schema
}

const (
	LeadStatePending   = "pending"
	LeadStateProcessed = "processed"
	LeadStateComplete  = "completed"
)

// Fields of the Lead.
func (Lead) Fields() []ent.Field {
	return []ent.Field{
		field.Enum("state").Values(LeadStatePending, LeadStateProcessed, LeadStateComplete).Default(LeadStatePending),
		field.Time("at").Default(func() time.Time {
			return time.Now()
		}),
		field.Time("schedule"),
	}
}

// Edges of the Lead.
func (Lead) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("user", User.Type).Ref("lead").Unique(),
		edge.From("event", Event.Type).Ref("lead").Unique(),
		edge.From("campaign", Campaign.Type).Ref("leads").Unique(),
		edge.From("touch_point", TouchPoint.Type).Ref("leads").Unique(),
	}
}
