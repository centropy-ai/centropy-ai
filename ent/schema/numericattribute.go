package schema

import (
	"github.com/facebookincubator/ent"

	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// NumericAttribute holds the schema definition for the NumericAttribute entity.
type NumericAttribute struct {
	ent.Schema
}

// Fields of the NumericAttribute.
func (NumericAttribute) Fields() []ent.Field {
	return []ent.Field{
		field.String("key"),
		field.Float("value"),
		field.String("itemId"),
		field.String("itemType"),
	}
}

// Edges of the NumericAttribute.
func (NumericAttribute) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("event", Event.Type).Ref("num_attr").Unique(),
	}
}
