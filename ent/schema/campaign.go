package schema

import (
	"time"

	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// Campaign holds the schema definition for the Campaign entity.
type Campaign struct {
	ent.Schema
}

// Fields of the Campaign.
func (Campaign) Fields() []ent.Field {
	return []ent.Field{
		field.Int8("tenant"),
		field.String("name").MinLen(5).MaxLen(128),
		field.String("description").MinLen(5).MaxLen(512),
		field.Time("release_at").Default(func() time.Time {
			return time.Now()
		}),
	}
}

// Edges of the Campaign.
func (Campaign) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("touch_points", TouchPoint.Type),
		edge.To("leads", Lead.Type),
	}
}
