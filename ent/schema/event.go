package schema

import (
	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// Event holds the schema definition for the Event entity.
type Event struct {
	ent.Schema
}

// Fields of the Event.
func (Event) Fields() []ent.Field {
	return []ent.Field{
		field.Int8("tenant"),
		field.Time("at"),
		field.String("name"),
		field.String("session_id"),
		field.String("scope"),
		field.String("itemId"),
	}
}

// Edges of the Event.
func (Event) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("str_attr", StringAttribute.Type),
		edge.To("num_attr", NumericAttribute.Type),
		edge.To("lead", Lead.Type),
		edge.From("by", User.Type).Ref("action").Unique(),
		edge.From("on", Entity.Type).Ref("event").Unique(),
	}
}
