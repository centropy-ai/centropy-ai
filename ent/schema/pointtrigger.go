package schema

import (
	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

type TriggerMetadata struct {
	Service string `json:"service"`
	Config  []byte `json:"config"`
}

// PointTrigger holds the schema definition for the PointTrigger entity.
type PointTrigger struct {
	ent.Schema
}

// Fields of the PointTrigger.
func (PointTrigger) Fields() []ent.Field {
	return []ent.Field{
		field.String("name"),
		field.String("profileID"),
		field.String("event"),
		field.Time("at"),
		field.JSON("action", &TriggerMetadata{}),
	}
}

// Edges of the PointTrigger.
func (PointTrigger) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("by", TouchPoint.Type).Ref("triggers").Unique(),
	}
}
