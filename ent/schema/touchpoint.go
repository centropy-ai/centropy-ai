package schema

import (
	"encoding/json"

	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// TouchPoint holds the schema definition for the TouchPoint entity.
type TouchPoint struct {
	ent.Schema
}

// Fields of the TouchPoint.
func (TouchPoint) Fields() []ent.Field {
	return []ent.Field{
		field.String("event"),
		field.Int("position").Default(1),
		field.Bool("optional").Default(false),
		field.Uint("duration_minutes").Default(0),
		field.JSON("condition", json.RawMessage{}),
	}
}

// Edges of the TouchPoint.
func (TouchPoint) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("triggers", PointTrigger.Type),
		edge.To("leads", Lead.Type),
		edge.To("yes", TouchPoint.Type).Unique(),
		edge.To("no", TouchPoint.Type).Unique(),
		edge.From("campaign", Campaign.Type).Ref("touch_points").Unique(),
	}
}
