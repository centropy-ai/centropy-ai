package schema

import (
	"github.com/facebookincubator/ent"
	"github.com/facebookincubator/ent/schema/edge"
	"github.com/facebookincubator/ent/schema/field"
)

// StringAttribute holds the schema definition for the StringAttribute entity.
type StringAttribute struct {
	ent.Schema
}

// Fields of the StringAttribute.
func (StringAttribute) Fields() []ent.Field {
	return []ent.Field{
		field.String("key"),
		field.String("value"),
		field.String("itemId"),
		field.String("itemType"),
	}
}

// Edges of the StringAttribute.
func (StringAttribute) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("event", Event.Type).Ref("str_attr").Unique(),
	}
}
