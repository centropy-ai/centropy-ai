// Code generated by entc, DO NOT EDIT.

package entity

const (
	// Label holds the string label denoting the entity type in the database.
	Label = "entity"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldName holds the string denoting the name vertex property in the database.
	FieldName = "name"
	// FieldCategory holds the string denoting the category vertex property in the database.
	FieldCategory = "category"

	// Table holds the table name of the entity in the database.
	Table = "entities"
	// EventTable is the table the holds the event relation/edge.
	EventTable = "events"
	// EventInverseTable is the table name for the Event entity.
	// It exists in this package in order to avoid circular dependency with the "event" package.
	EventInverseTable = "events"
	// EventColumn is the table column denoting the event relation/edge.
	EventColumn = "entity_event"
)

// Columns holds all SQL columns for entity fields.
var Columns = []string{
	FieldID,
	FieldName,
	FieldCategory,
}
