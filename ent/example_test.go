// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"log"
	"time"

	"github.com/facebookincubator/ent/dialect/sql"

	"gitlab.com/centropy-ai/centropy-ai/ent/lead"
)

// dsn for the database. In order to run the tests locally, run the following command:
//
//	 ENT_INTEGRATION_ENDPOINT="root:pass@tcp(localhost:3306)/test?parseTime=True" go test -v
//
var dsn string

func ExampleCampaign() {
	if dsn == "" {
		return
	}
	ctx := context.Background()
	drv, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("failed creating database client: %v", err)
	}
	defer drv.Close()
	client := NewClient(Driver(drv))
	// creating vertices for the campaign's edges.
	tp0 := client.TouchPoint.
		Create().
		SetEvent("string").
		SetPosition(1).
		SetOptional(true).
		SetDurationMinutes(1).
		SetCondition(nil).
		SaveX(ctx)
	log.Println("touchpoint created:", tp0)
	l1 := client.Lead.
		Create().
		SetState(lead.StatePending).
		SetAt(time.Now()).
		SetSchedule(time.Now()).
		SaveX(ctx)
	log.Println("lead created:", l1)

	// create campaign vertex with its edges.
	c := client.Campaign.
		Create().
		SetTenant(1).
		SetName("string").
		SetDescription("string").
		SetReleaseAt(time.Now()).
		AddTouchPoints(tp0).
		AddLeads(l1).
		SaveX(ctx)
	log.Println("campaign created:", c)

	// query edges.
	tp0, err = c.QueryTouchPoints().First(ctx)
	if err != nil {
		log.Fatalf("failed querying touch_points: %v", err)
	}
	log.Println("touch_points found:", tp0)

	l1, err = c.QueryLeads().First(ctx)
	if err != nil {
		log.Fatalf("failed querying leads: %v", err)
	}
	log.Println("leads found:", l1)

	// Output:
}
func ExampleEntity() {
	if dsn == "" {
		return
	}
	ctx := context.Background()
	drv, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("failed creating database client: %v", err)
	}
	defer drv.Close()
	client := NewClient(Driver(drv))
	// creating vertices for the entity's edges.
	e0 := client.Event.
		Create().
		SetTenant(1).
		SetAt(time.Now()).
		SetName("string").
		SetSessionID("string").
		SetScope("string").
		SetItemId("string").
		SaveX(ctx)
	log.Println("event created:", e0)

	// create entity vertex with its edges.
	e := client.Entity.
		Create().
		SetName("string").
		SetCategory("string").
		AddEvent(e0).
		SaveX(ctx)
	log.Println("entity created:", e)

	// query edges.
	e0, err = e.QueryEvent().First(ctx)
	if err != nil {
		log.Fatalf("failed querying event: %v", err)
	}
	log.Println("event found:", e0)

	// Output:
}
func ExampleEvent() {
	if dsn == "" {
		return
	}
	ctx := context.Background()
	drv, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("failed creating database client: %v", err)
	}
	defer drv.Close()
	client := NewClient(Driver(drv))
	// creating vertices for the event's edges.
	sa0 := client.StringAttribute.
		Create().
		SetKey("string").
		SetValue("string").
		SetItemId("string").
		SetItemType("string").
		SaveX(ctx)
	log.Println("stringattribute created:", sa0)
	na1 := client.NumericAttribute.
		Create().
		SetKey("string").
		SetValue(1).
		SetItemId("string").
		SetItemType("string").
		SaveX(ctx)
	log.Println("numericattribute created:", na1)
	l2 := client.Lead.
		Create().
		SetState(lead.StatePending).
		SetAt(time.Now()).
		SetSchedule(time.Now()).
		SaveX(ctx)
	log.Println("lead created:", l2)

	// create event vertex with its edges.
	e := client.Event.
		Create().
		SetTenant(1).
		SetAt(time.Now()).
		SetName("string").
		SetSessionID("string").
		SetScope("string").
		SetItemId("string").
		AddStrAttr(sa0).
		AddNumAttr(na1).
		AddLead(l2).
		SaveX(ctx)
	log.Println("event created:", e)

	// query edges.
	sa0, err = e.QueryStrAttr().First(ctx)
	if err != nil {
		log.Fatalf("failed querying str_attr: %v", err)
	}
	log.Println("str_attr found:", sa0)

	na1, err = e.QueryNumAttr().First(ctx)
	if err != nil {
		log.Fatalf("failed querying num_attr: %v", err)
	}
	log.Println("num_attr found:", na1)

	l2, err = e.QueryLead().First(ctx)
	if err != nil {
		log.Fatalf("failed querying lead: %v", err)
	}
	log.Println("lead found:", l2)

	// Output:
}
func ExampleLead() {
	if dsn == "" {
		return
	}
	ctx := context.Background()
	drv, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("failed creating database client: %v", err)
	}
	defer drv.Close()
	client := NewClient(Driver(drv))
	// creating vertices for the lead's edges.

	// create lead vertex with its edges.
	l := client.Lead.
		Create().
		SetState(lead.StatePending).
		SetAt(time.Now()).
		SetSchedule(time.Now()).
		SaveX(ctx)
	log.Println("lead created:", l)

	// query edges.

	// Output:
}
func ExampleNumericAttribute() {
	if dsn == "" {
		return
	}
	ctx := context.Background()
	drv, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("failed creating database client: %v", err)
	}
	defer drv.Close()
	client := NewClient(Driver(drv))
	// creating vertices for the numericattribute's edges.

	// create numericattribute vertex with its edges.
	na := client.NumericAttribute.
		Create().
		SetKey("string").
		SetValue(1).
		SetItemId("string").
		SetItemType("string").
		SaveX(ctx)
	log.Println("numericattribute created:", na)

	// query edges.

	// Output:
}
func ExamplePointTrigger() {
	if dsn == "" {
		return
	}
	ctx := context.Background()
	drv, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("failed creating database client: %v", err)
	}
	defer drv.Close()
	client := NewClient(Driver(drv))
	// creating vertices for the pointtrigger's edges.

	// create pointtrigger vertex with its edges.
	pt := client.PointTrigger.
		Create().
		SetName("string").
		SetAction(nil).
		SaveX(ctx)
	log.Println("pointtrigger created:", pt)

	// query edges.

	// Output:
}
func ExampleStringAttribute() {
	if dsn == "" {
		return
	}
	ctx := context.Background()
	drv, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("failed creating database client: %v", err)
	}
	defer drv.Close()
	client := NewClient(Driver(drv))
	// creating vertices for the stringattribute's edges.

	// create stringattribute vertex with its edges.
	sa := client.StringAttribute.
		Create().
		SetKey("string").
		SetValue("string").
		SetItemId("string").
		SetItemType("string").
		SaveX(ctx)
	log.Println("stringattribute created:", sa)

	// query edges.

	// Output:
}
func ExampleTouchPoint() {
	if dsn == "" {
		return
	}
	ctx := context.Background()
	drv, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("failed creating database client: %v", err)
	}
	defer drv.Close()
	client := NewClient(Driver(drv))
	// creating vertices for the touchpoint's edges.
	pt0 := client.PointTrigger.
		Create().
		SetName("string").
		SetAction(nil).
		SaveX(ctx)
	log.Println("pointtrigger created:", pt0)
	l1 := client.Lead.
		Create().
		SetState(lead.StatePending).
		SetAt(time.Now()).
		SetSchedule(time.Now()).
		SaveX(ctx)
	log.Println("lead created:", l1)
	tp2 := client.TouchPoint.
		Create().
		SetEvent("string").
		SetPosition(1).
		SetOptional(true).
		SetDurationMinutes(1).
		SetCondition(nil).
		SaveX(ctx)
	log.Println("touchpoint created:", tp2)
	tp3 := client.TouchPoint.
		Create().
		SetEvent("string").
		SetPosition(1).
		SetOptional(true).
		SetDurationMinutes(1).
		SetCondition(nil).
		SaveX(ctx)
	log.Println("touchpoint created:", tp3)

	// create touchpoint vertex with its edges.
	tp := client.TouchPoint.
		Create().
		SetEvent("string").
		SetPosition(1).
		SetOptional(true).
		SetDurationMinutes(1).
		SetCondition(nil).
		AddTriggers(pt0).
		AddLeads(l1).
		SetYes(tp2).
		SetNo(tp3).
		SaveX(ctx)
	log.Println("touchpoint created:", tp)

	// query edges.
	pt0, err = tp.QueryTriggers().First(ctx)
	if err != nil {
		log.Fatalf("failed querying triggers: %v", err)
	}
	log.Println("triggers found:", pt0)

	l1, err = tp.QueryLeads().First(ctx)
	if err != nil {
		log.Fatalf("failed querying leads: %v", err)
	}
	log.Println("leads found:", l1)

	tp2, err = tp.QueryYes().First(ctx)
	if err != nil {
		log.Fatalf("failed querying yes: %v", err)
	}
	log.Println("yes found:", tp2)

	tp3, err = tp.QueryNo().First(ctx)
	if err != nil {
		log.Fatalf("failed querying no: %v", err)
	}
	log.Println("no found:", tp3)

	// Output:
}
func ExampleUser() {
	if dsn == "" {
		return
	}
	ctx := context.Background()
	drv, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("failed creating database client: %v", err)
	}
	defer drv.Close()
	client := NewClient(Driver(drv))
	// creating vertices for the user's edges.
	l0 := client.Lead.
		Create().
		SetState(lead.StatePending).
		SetAt(time.Now()).
		SetSchedule(time.Now()).
		SaveX(ctx)
	log.Println("lead created:", l0)
	e1 := client.Event.
		Create().
		SetTenant(1).
		SetAt(time.Now()).
		SetName("string").
		SetSessionID("string").
		SetScope("string").
		SetItemId("string").
		SaveX(ctx)
	log.Println("event created:", e1)

	// create user vertex with its edges.
	u := client.User.
		Create().
		SetUserID("string").
		SetEmail(nil).
		SetSessionIds(nil).
		AddLead(l0).
		AddAction(e1).
		SaveX(ctx)
	log.Println("user created:", u)

	// query edges.
	l0, err = u.QueryLead().First(ctx)
	if err != nil {
		log.Fatalf("failed querying lead: %v", err)
	}
	log.Println("lead found:", l0)

	e1, err = u.QueryAction().First(ctx)
	if err != nil {
		log.Fatalf("failed querying action: %v", err)
	}
	log.Println("action found:", e1)

	// Output:
}
