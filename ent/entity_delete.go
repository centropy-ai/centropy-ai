// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"

	"github.com/facebookincubator/ent/dialect/sql"
	"github.com/facebookincubator/ent/dialect/sql/sqlgraph"
	"github.com/facebookincubator/ent/schema/field"
	"gitlab.com/centropy-ai/centropy-ai/ent/entity"
	"gitlab.com/centropy-ai/centropy-ai/ent/predicate"
)

// EntityDelete is the builder for deleting a Entity entity.
type EntityDelete struct {
	config
	predicates []predicate.Entity
}

// Where adds a new predicate to the delete builder.
func (ed *EntityDelete) Where(ps ...predicate.Entity) *EntityDelete {
	ed.predicates = append(ed.predicates, ps...)
	return ed
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (ed *EntityDelete) Exec(ctx context.Context) (int, error) {
	return ed.sqlExec(ctx)
}

// ExecX is like Exec, but panics if an error occurs.
func (ed *EntityDelete) ExecX(ctx context.Context) int {
	n, err := ed.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (ed *EntityDelete) sqlExec(ctx context.Context) (int, error) {
	_spec := &sqlgraph.DeleteSpec{
		Node: &sqlgraph.NodeSpec{
			Table: entity.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: entity.FieldID,
			},
		},
	}
	if ps := ed.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return sqlgraph.DeleteNodes(ctx, ed.driver, _spec)
}

// EntityDeleteOne is the builder for deleting a single Entity entity.
type EntityDeleteOne struct {
	ed *EntityDelete
}

// Exec executes the deletion query.
func (edo *EntityDeleteOne) Exec(ctx context.Context) error {
	n, err := edo.ed.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &NotFoundError{entity.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (edo *EntityDeleteOne) ExecX(ctx context.Context) {
	edo.ed.ExecX(ctx)
}
