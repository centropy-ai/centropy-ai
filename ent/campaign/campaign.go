// Code generated by entc, DO NOT EDIT.

package campaign

import (
	"time"

	"gitlab.com/centropy-ai/centropy-ai/ent/schema"
)

const (
	// Label holds the string label denoting the campaign type in the database.
	Label = "campaign"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldTenant holds the string denoting the tenant vertex property in the database.
	FieldTenant = "tenant"
	// FieldName holds the string denoting the name vertex property in the database.
	FieldName = "name"
	// FieldDescription holds the string denoting the description vertex property in the database.
	FieldDescription = "description"
	// FieldReleaseAt holds the string denoting the release_at vertex property in the database.
	FieldReleaseAt = "release_at"

	// Table holds the table name of the campaign in the database.
	Table = "campaigns"
	// TouchPointsTable is the table the holds the touch_points relation/edge.
	TouchPointsTable = "touch_points"
	// TouchPointsInverseTable is the table name for the TouchPoint entity.
	// It exists in this package in order to avoid circular dependency with the "touchpoint" package.
	TouchPointsInverseTable = "touch_points"
	// TouchPointsColumn is the table column denoting the touch_points relation/edge.
	TouchPointsColumn = "campaign_touch_points"
	// LeadsTable is the table the holds the leads relation/edge.
	LeadsTable = "leads"
	// LeadsInverseTable is the table name for the Lead entity.
	// It exists in this package in order to avoid circular dependency with the "lead" package.
	LeadsInverseTable = "leads"
	// LeadsColumn is the table column denoting the leads relation/edge.
	LeadsColumn = "campaign_leads"
)

// Columns holds all SQL columns for campaign fields.
var Columns = []string{
	FieldID,
	FieldTenant,
	FieldName,
	FieldDescription,
	FieldReleaseAt,
}

var (
	fields = schema.Campaign{}.Fields()

	// descName is the schema descriptor for name field.
	descName = fields[1].Descriptor()
	// NameValidator is a validator for the "name" field. It is called by the builders before save.
	NameValidator = func() func(string) error {
		validators := descName.Validators
		fns := [...]func(string) error{
			validators[0].(func(string) error),
			validators[1].(func(string) error),
		}
		return func(name string) error {
			for _, fn := range fns {
				if err := fn(name); err != nil {
					return err
				}
			}
			return nil
		}
	}()

	// descDescription is the schema descriptor for description field.
	descDescription = fields[2].Descriptor()
	// DescriptionValidator is a validator for the "description" field. It is called by the builders before save.
	DescriptionValidator = func() func(string) error {
		validators := descDescription.Validators
		fns := [...]func(string) error{
			validators[0].(func(string) error),
			validators[1].(func(string) error),
		}
		return func(description string) error {
			for _, fn := range fns {
				if err := fn(description); err != nil {
					return err
				}
			}
			return nil
		}
	}()

	// descReleaseAt is the schema descriptor for release_at field.
	descReleaseAt = fields[3].Descriptor()
	// DefaultReleaseAt holds the default value on creation for the release_at field.
	DefaultReleaseAt = descReleaseAt.Default.(func() time.Time)
)
