// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"math"

	"github.com/facebookincubator/ent/dialect/sql"
	"github.com/facebookincubator/ent/dialect/sql/sqlgraph"
	"github.com/facebookincubator/ent/schema/field"
	"gitlab.com/centropy-ai/centropy-ai/ent/event"
	"gitlab.com/centropy-ai/centropy-ai/ent/predicate"
	"gitlab.com/centropy-ai/centropy-ai/ent/stringattribute"
)

// StringAttributeQuery is the builder for querying StringAttribute entities.
type StringAttributeQuery struct {
	config
	limit      *int
	offset     *int
	order      []Order
	unique     []string
	predicates []predicate.StringAttribute
	// eager-loading edges.
	withEvent *EventQuery
	withFKs   bool
	// intermediate query.
	sql *sql.Selector
}

// Where adds a new predicate for the builder.
func (saq *StringAttributeQuery) Where(ps ...predicate.StringAttribute) *StringAttributeQuery {
	saq.predicates = append(saq.predicates, ps...)
	return saq
}

// Limit adds a limit step to the query.
func (saq *StringAttributeQuery) Limit(limit int) *StringAttributeQuery {
	saq.limit = &limit
	return saq
}

// Offset adds an offset step to the query.
func (saq *StringAttributeQuery) Offset(offset int) *StringAttributeQuery {
	saq.offset = &offset
	return saq
}

// Order adds an order step to the query.
func (saq *StringAttributeQuery) Order(o ...Order) *StringAttributeQuery {
	saq.order = append(saq.order, o...)
	return saq
}

// QueryEvent chains the current query on the event edge.
func (saq *StringAttributeQuery) QueryEvent() *EventQuery {
	query := &EventQuery{config: saq.config}
	step := sqlgraph.NewStep(
		sqlgraph.From(stringattribute.Table, stringattribute.FieldID, saq.sqlQuery()),
		sqlgraph.To(event.Table, event.FieldID),
		sqlgraph.Edge(sqlgraph.M2O, true, stringattribute.EventTable, stringattribute.EventColumn),
	)
	query.sql = sqlgraph.SetNeighbors(saq.driver.Dialect(), step)
	return query
}

// First returns the first StringAttribute entity in the query. Returns *NotFoundError when no stringattribute was found.
func (saq *StringAttributeQuery) First(ctx context.Context) (*StringAttribute, error) {
	sas, err := saq.Limit(1).All(ctx)
	if err != nil {
		return nil, err
	}
	if len(sas) == 0 {
		return nil, &NotFoundError{stringattribute.Label}
	}
	return sas[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (saq *StringAttributeQuery) FirstX(ctx context.Context) *StringAttribute {
	sa, err := saq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return sa
}

// FirstID returns the first StringAttribute id in the query. Returns *NotFoundError when no id was found.
func (saq *StringAttributeQuery) FirstID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = saq.Limit(1).IDs(ctx); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{stringattribute.Label}
		return
	}
	return ids[0], nil
}

// FirstXID is like FirstID, but panics if an error occurs.
func (saq *StringAttributeQuery) FirstXID(ctx context.Context) int {
	id, err := saq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns the only StringAttribute entity in the query, returns an error if not exactly one entity was returned.
func (saq *StringAttributeQuery) Only(ctx context.Context) (*StringAttribute, error) {
	sas, err := saq.Limit(2).All(ctx)
	if err != nil {
		return nil, err
	}
	switch len(sas) {
	case 1:
		return sas[0], nil
	case 0:
		return nil, &NotFoundError{stringattribute.Label}
	default:
		return nil, &NotSingularError{stringattribute.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (saq *StringAttributeQuery) OnlyX(ctx context.Context) *StringAttribute {
	sa, err := saq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return sa
}

// OnlyID returns the only StringAttribute id in the query, returns an error if not exactly one id was returned.
func (saq *StringAttributeQuery) OnlyID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = saq.Limit(2).IDs(ctx); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{stringattribute.Label}
	default:
		err = &NotSingularError{stringattribute.Label}
	}
	return
}

// OnlyXID is like OnlyID, but panics if an error occurs.
func (saq *StringAttributeQuery) OnlyXID(ctx context.Context) int {
	id, err := saq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of StringAttributes.
func (saq *StringAttributeQuery) All(ctx context.Context) ([]*StringAttribute, error) {
	return saq.sqlAll(ctx)
}

// AllX is like All, but panics if an error occurs.
func (saq *StringAttributeQuery) AllX(ctx context.Context) []*StringAttribute {
	sas, err := saq.All(ctx)
	if err != nil {
		panic(err)
	}
	return sas
}

// IDs executes the query and returns a list of StringAttribute ids.
func (saq *StringAttributeQuery) IDs(ctx context.Context) ([]int, error) {
	var ids []int
	if err := saq.Select(stringattribute.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (saq *StringAttributeQuery) IDsX(ctx context.Context) []int {
	ids, err := saq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (saq *StringAttributeQuery) Count(ctx context.Context) (int, error) {
	return saq.sqlCount(ctx)
}

// CountX is like Count, but panics if an error occurs.
func (saq *StringAttributeQuery) CountX(ctx context.Context) int {
	count, err := saq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (saq *StringAttributeQuery) Exist(ctx context.Context) (bool, error) {
	return saq.sqlExist(ctx)
}

// ExistX is like Exist, but panics if an error occurs.
func (saq *StringAttributeQuery) ExistX(ctx context.Context) bool {
	exist, err := saq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the query builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (saq *StringAttributeQuery) Clone() *StringAttributeQuery {
	return &StringAttributeQuery{
		config:     saq.config,
		limit:      saq.limit,
		offset:     saq.offset,
		order:      append([]Order{}, saq.order...),
		unique:     append([]string{}, saq.unique...),
		predicates: append([]predicate.StringAttribute{}, saq.predicates...),
		// clone intermediate query.
		sql: saq.sql.Clone(),
	}
}

//  WithEvent tells the query-builder to eager-loads the nodes that are connected to
// the "event" edge. The optional arguments used to configure the query builder of the edge.
func (saq *StringAttributeQuery) WithEvent(opts ...func(*EventQuery)) *StringAttributeQuery {
	query := &EventQuery{config: saq.config}
	for _, opt := range opts {
		opt(query)
	}
	saq.withEvent = query
	return saq
}

// GroupBy used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
//
// Example:
//
//	var v []struct {
//		Key string `json:"key,omitempty"`
//		Count int `json:"count,omitempty"`
//	}
//
//	client.StringAttribute.Query().
//		GroupBy(stringattribute.FieldKey).
//		Aggregate(ent.Count()).
//		Scan(ctx, &v)
//
func (saq *StringAttributeQuery) GroupBy(field string, fields ...string) *StringAttributeGroupBy {
	group := &StringAttributeGroupBy{config: saq.config}
	group.fields = append([]string{field}, fields...)
	group.sql = saq.sqlQuery()
	return group
}

// Select one or more fields from the given query.
//
// Example:
//
//	var v []struct {
//		Key string `json:"key,omitempty"`
//	}
//
//	client.StringAttribute.Query().
//		Select(stringattribute.FieldKey).
//		Scan(ctx, &v)
//
func (saq *StringAttributeQuery) Select(field string, fields ...string) *StringAttributeSelect {
	selector := &StringAttributeSelect{config: saq.config}
	selector.fields = append([]string{field}, fields...)
	selector.sql = saq.sqlQuery()
	return selector
}

func (saq *StringAttributeQuery) sqlAll(ctx context.Context) ([]*StringAttribute, error) {
	var (
		nodes       = []*StringAttribute{}
		withFKs     = saq.withFKs
		_spec       = saq.querySpec()
		loadedTypes = [1]bool{
			saq.withEvent != nil,
		}
	)
	if saq.withEvent != nil {
		withFKs = true
	}
	if withFKs {
		_spec.Node.Columns = append(_spec.Node.Columns, stringattribute.ForeignKeys...)
	}
	_spec.ScanValues = func() []interface{} {
		node := &StringAttribute{config: saq.config}
		nodes = append(nodes, node)
		values := node.scanValues()
		if withFKs {
			values = append(values, node.fkValues()...)
		}
		return values
	}
	_spec.Assign = func(values ...interface{}) error {
		if len(nodes) == 0 {
			return fmt.Errorf("ent: Assign called without calling ScanValues")
		}
		node := nodes[len(nodes)-1]
		node.Edges.loadedTypes = loadedTypes
		return node.assignValues(values...)
	}
	if err := sqlgraph.QueryNodes(ctx, saq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}

	if query := saq.withEvent; query != nil {
		ids := make([]int, 0, len(nodes))
		nodeids := make(map[int][]*StringAttribute)
		for i := range nodes {
			if fk := nodes[i].event_str_attr; fk != nil {
				ids = append(ids, *fk)
				nodeids[*fk] = append(nodeids[*fk], nodes[i])
			}
		}
		query.Where(event.IDIn(ids...))
		neighbors, err := query.All(ctx)
		if err != nil {
			return nil, err
		}
		for _, n := range neighbors {
			nodes, ok := nodeids[n.ID]
			if !ok {
				return nil, fmt.Errorf(`unexpected foreign-key "event_str_attr" returned %v`, n.ID)
			}
			for i := range nodes {
				nodes[i].Edges.Event = n
			}
		}
	}

	return nodes, nil
}

func (saq *StringAttributeQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := saq.querySpec()
	return sqlgraph.CountNodes(ctx, saq.driver, _spec)
}

func (saq *StringAttributeQuery) sqlExist(ctx context.Context) (bool, error) {
	n, err := saq.sqlCount(ctx)
	if err != nil {
		return false, fmt.Errorf("ent: check existence: %v", err)
	}
	return n > 0, nil
}

func (saq *StringAttributeQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := &sqlgraph.QuerySpec{
		Node: &sqlgraph.NodeSpec{
			Table:   stringattribute.Table,
			Columns: stringattribute.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: stringattribute.FieldID,
			},
		},
		From:   saq.sql,
		Unique: true,
	}
	if ps := saq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := saq.limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := saq.offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := saq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (saq *StringAttributeQuery) sqlQuery() *sql.Selector {
	builder := sql.Dialect(saq.driver.Dialect())
	t1 := builder.Table(stringattribute.Table)
	selector := builder.Select(t1.Columns(stringattribute.Columns...)...).From(t1)
	if saq.sql != nil {
		selector = saq.sql
		selector.Select(selector.Columns(stringattribute.Columns...)...)
	}
	for _, p := range saq.predicates {
		p(selector)
	}
	for _, p := range saq.order {
		p(selector)
	}
	if offset := saq.offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := saq.limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// StringAttributeGroupBy is the builder for group-by StringAttribute entities.
type StringAttributeGroupBy struct {
	config
	fields []string
	fns    []Aggregate
	// intermediate query.
	sql *sql.Selector
}

// Aggregate adds the given aggregation functions to the group-by query.
func (sagb *StringAttributeGroupBy) Aggregate(fns ...Aggregate) *StringAttributeGroupBy {
	sagb.fns = append(sagb.fns, fns...)
	return sagb
}

// Scan applies the group-by query and scan the result into the given value.
func (sagb *StringAttributeGroupBy) Scan(ctx context.Context, v interface{}) error {
	return sagb.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (sagb *StringAttributeGroupBy) ScanX(ctx context.Context, v interface{}) {
	if err := sagb.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from group-by. It is only allowed when querying group-by with one field.
func (sagb *StringAttributeGroupBy) Strings(ctx context.Context) ([]string, error) {
	if len(sagb.fields) > 1 {
		return nil, errors.New("ent: StringAttributeGroupBy.Strings is not achievable when grouping more than 1 field")
	}
	var v []string
	if err := sagb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (sagb *StringAttributeGroupBy) StringsX(ctx context.Context) []string {
	v, err := sagb.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from group-by. It is only allowed when querying group-by with one field.
func (sagb *StringAttributeGroupBy) Ints(ctx context.Context) ([]int, error) {
	if len(sagb.fields) > 1 {
		return nil, errors.New("ent: StringAttributeGroupBy.Ints is not achievable when grouping more than 1 field")
	}
	var v []int
	if err := sagb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (sagb *StringAttributeGroupBy) IntsX(ctx context.Context) []int {
	v, err := sagb.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from group-by. It is only allowed when querying group-by with one field.
func (sagb *StringAttributeGroupBy) Float64s(ctx context.Context) ([]float64, error) {
	if len(sagb.fields) > 1 {
		return nil, errors.New("ent: StringAttributeGroupBy.Float64s is not achievable when grouping more than 1 field")
	}
	var v []float64
	if err := sagb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (sagb *StringAttributeGroupBy) Float64sX(ctx context.Context) []float64 {
	v, err := sagb.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from group-by. It is only allowed when querying group-by with one field.
func (sagb *StringAttributeGroupBy) Bools(ctx context.Context) ([]bool, error) {
	if len(sagb.fields) > 1 {
		return nil, errors.New("ent: StringAttributeGroupBy.Bools is not achievable when grouping more than 1 field")
	}
	var v []bool
	if err := sagb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (sagb *StringAttributeGroupBy) BoolsX(ctx context.Context) []bool {
	v, err := sagb.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (sagb *StringAttributeGroupBy) sqlScan(ctx context.Context, v interface{}) error {
	rows := &sql.Rows{}
	query, args := sagb.sqlQuery().Query()
	if err := sagb.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (sagb *StringAttributeGroupBy) sqlQuery() *sql.Selector {
	selector := sagb.sql
	columns := make([]string, 0, len(sagb.fields)+len(sagb.fns))
	columns = append(columns, sagb.fields...)
	for _, fn := range sagb.fns {
		columns = append(columns, fn(selector))
	}
	return selector.Select(columns...).GroupBy(sagb.fields...)
}

// StringAttributeSelect is the builder for select fields of StringAttribute entities.
type StringAttributeSelect struct {
	config
	fields []string
	// intermediate queries.
	sql *sql.Selector
}

// Scan applies the selector query and scan the result into the given value.
func (sas *StringAttributeSelect) Scan(ctx context.Context, v interface{}) error {
	return sas.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (sas *StringAttributeSelect) ScanX(ctx context.Context, v interface{}) {
	if err := sas.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from selector. It is only allowed when selecting one field.
func (sas *StringAttributeSelect) Strings(ctx context.Context) ([]string, error) {
	if len(sas.fields) > 1 {
		return nil, errors.New("ent: StringAttributeSelect.Strings is not achievable when selecting more than 1 field")
	}
	var v []string
	if err := sas.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (sas *StringAttributeSelect) StringsX(ctx context.Context) []string {
	v, err := sas.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from selector. It is only allowed when selecting one field.
func (sas *StringAttributeSelect) Ints(ctx context.Context) ([]int, error) {
	if len(sas.fields) > 1 {
		return nil, errors.New("ent: StringAttributeSelect.Ints is not achievable when selecting more than 1 field")
	}
	var v []int
	if err := sas.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (sas *StringAttributeSelect) IntsX(ctx context.Context) []int {
	v, err := sas.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from selector. It is only allowed when selecting one field.
func (sas *StringAttributeSelect) Float64s(ctx context.Context) ([]float64, error) {
	if len(sas.fields) > 1 {
		return nil, errors.New("ent: StringAttributeSelect.Float64s is not achievable when selecting more than 1 field")
	}
	var v []float64
	if err := sas.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (sas *StringAttributeSelect) Float64sX(ctx context.Context) []float64 {
	v, err := sas.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from selector. It is only allowed when selecting one field.
func (sas *StringAttributeSelect) Bools(ctx context.Context) ([]bool, error) {
	if len(sas.fields) > 1 {
		return nil, errors.New("ent: StringAttributeSelect.Bools is not achievable when selecting more than 1 field")
	}
	var v []bool
	if err := sas.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (sas *StringAttributeSelect) BoolsX(ctx context.Context) []bool {
	v, err := sas.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (sas *StringAttributeSelect) sqlScan(ctx context.Context, v interface{}) error {
	rows := &sql.Rows{}
	query, args := sas.sqlQuery().Query()
	if err := sas.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (sas *StringAttributeSelect) sqlQuery() sql.Querier {
	selector := sas.sql
	selector.Select(selector.Columns(sas.fields...)...)
	return selector
}
