package main

import (
	"gitlab.com/centropy-ai/centropy-ai/cmd"
	_ "gitlab.com/centropy-ai/centropy-ai/pkg/cmd"
)

func main() {
	cmd.Execute()
}
