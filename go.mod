module gitlab.com/centropy-ai/centropy-ai

go 1.12

require (
	github.com/bsm/sarama-cluster v2.1.15+incompatible // indirect
	github.com/capitalone/go-future-context v0.0.0-20190710004304-a898d890006f
	github.com/carlescere/scheduler v0.0.0-20170109141437-ee74d2f83d82
	github.com/dgraph-io/ristretto v0.0.1
	github.com/facebookgo/ensure v0.0.0-20160127193407-b4ab57deab51 // indirect
	github.com/facebookgo/inject v0.0.0-20180706035515-f23751cae28b
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/structtag v0.0.0-20150214074306-217e25fb9691 // indirect
	github.com/facebookgo/subset v0.0.0-20150612182917-8dac2c3c4870 // indirect
	github.com/facebookincubator/ent v0.1.4
	github.com/gin-gonic/gin v1.5.0
	github.com/gogo/status v1.1.0
	github.com/golang/protobuf v1.3.2
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/grpc-gateway v1.9.0
	github.com/hashicorp/consul/api v1.3.0
	github.com/hashicorp/golang-lru v0.5.3
	github.com/hashicorp/memberlist v0.1.5
	github.com/jinzhu/gorm v1.9.11
	github.com/jonbodner/ranger v0.0.0-20170219221806-de0a8243f007 // indirect
	github.com/lovoo/goka v0.1.4
	github.com/mitchellh/mapstructure v1.1.2
	github.com/oschwald/geoip2-golang v1.4.0
	github.com/samuel/go-zookeeper v0.0.0-20190923202752-2cc03de413da // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.6.1
	github.com/stretchr/testify v1.4.0
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/ua-parser/uap-go v0.0.0-20200325213135-e1c09f13e2fe
	github.com/wvanbergen/kazoo-go v0.0.0-20180202103751-f72d8611297a // indirect
	github.com/xrash/smetrics v0.0.0-20170218160415-a3153f7040e9
	go.elastic.co/apm/module/apmzap v1.6.0
	go.uber.org/zap v1.13.0
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543
	google.golang.org/genproto v0.0.0-20190425155659-357c62f0e4bb
	google.golang.org/grpc v1.21.0
	gopkg.in/gormigrate.v1 v1.6.0
	gopkg.in/h2non/gock.v1 v1.0.15
)
